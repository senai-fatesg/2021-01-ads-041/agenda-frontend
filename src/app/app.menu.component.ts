import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-menu',
    template: `
        <div class="menu-scroll-content">
			<ul class="navigation-menu">
				<li app-menuitem *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true"></li>
			</ul>
        </div>
    `
})
export class AppMenuComponent implements OnInit {

    public model: any[];

    ngOnInit() {
        this.model = [
            {label: 'Painel de Bordo', icon: 'pi pi-fw pi-star', routerLink: ['/']},
            {
                label: 'Cadastro', icon: 'pi pi-fw pi-pencil', routerLink: ['/uikit'], 
                items: [
                    {label: 'Pessoas', icon: 'pi pi-fw pi-user', routerLink: ['/pessoa-list']},
                    {label: 'Locais de Interesse', icon: 'pi pi-fw pi-map-marker', routerLink: ['/uikit/input']},
                ]
            }, 
            {
                label: 'Compromissos', icon: 'pi pi-fw pi-compass', routerLink: ['utilities'], badge: 2,
                items: [
                    {label: 'Novo', icon: 'pi pi-fw pi-desktop', routerLink: ['utilities/display']},
                    {label: 'Listar', icon: 'pi pi-fw pi-external-link', routerLink: ['utilities/elevation']},
                ]
            },
        ];
    }
}
