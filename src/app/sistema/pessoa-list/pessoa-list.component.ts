import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PessoaService } from 'src/app/pessoa.service';
import { Pessoa } from '../pessoa/pessoa.component';

@Component({
  selector: 'app-pessoa-list',
  templateUrl: './pessoa-list.component.html',
  styleUrls: ['./pessoa-list.component.scss']
})
export class PessoaListComponent implements OnInit {

  pessoas:any = [];
  pessoaSelecionada:Pessoa;

  constructor(private router: Router, private pessoaService: PessoaService) { }

  ngOnInit(): void {
    this.pessoaService.get().subscribe(r => {
      this.pessoas = r;
    });
  }

  btnNova(){
    this.router.navigateByUrl('/pessoa/nova');
  }

  onRowSelect(event){
    console.log(event);
  }

  selectPessoa(pessoa){
    console.log(pessoa);
    this.router.navigateByUrl(`/pessoa/${pessoa.id}`);
  }

}
